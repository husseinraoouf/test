FROM node:13-alpine AS build

# Set working directory. Paths will be relative this WORKDIR.
WORKDIR /app

# Install Debendencies as a seperate layer for caching
# Install dependencies
COPY package*.json ./
RUN npm install --production >/dev/null

# generate prisma client as a seperate layer for caching
# Copy source files from host computer to the container
COPY prisma ./prisma
RUN npm install --global @prisma/cli@2.0.0-alpha.1272 >/dev/null && prisma generate >/dev/null

# Copy source files from host computer to the container
COPY . .


# Test Image ( contains the runtime dependencies and testing dependencies )
# Workaround to install and run both postgresql and node in the same image
# Should be handled better or at least abstractd in an image
FROM postgres:12-alpine AS test

RUN apk update && apk add nodejs nodejs-npm
RUN npm install --global @prisma/cli@2.0.0-alpha.1272 >/dev/null

# Set working directory. Paths will be relative this WORKDIR.
WORKDIR /app

# Copy source files from host computer to the container
COPY --from=build /app .

# Install dev dependncies
RUN npm install --only=dev >/dev/null

# Allow to connect to PostgreSQL without password
# it is normally not acceptable to do this but it is justfied here because i only use PostgreSQL for testing
ENV POSTGRES_HOST_AUTH_METHOD=trust

# Run the app
# workaround to be able to run multible command with '&&'
CMD [ "sh", "-c", "docker-entrypoint.sh postgres &>/dev/null & (sleep 3 && npm test -- --coverage)"]


# Releas Image ( contains the runtime dependencies only )
FROM node:13-alpine AS release

# Set working directory. Paths will be relative this WORKDIR.
WORKDIR /app

# Copy source files from host computer to the container
COPY --from=build /app .

# Set the default active environment to production
ENV ACTIVE_ENVIRONMENT=Production

# Run the app
CMD [ "npm", "start" ]