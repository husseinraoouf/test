{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {

    # Installed Packages
    buildInputs = [ 
        pkgs.nodejs-13_x
        pkgs.nur.repos.husseinraoouf.node-packages."@prisma/cli"
    ];

    # Enviroment Variables
    DOCKER_BUILDKIT = 1;
}
