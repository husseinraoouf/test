
<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
  * [Project Structure](#project-structure)
  * [Process](#process)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
* [Usage](#usage)
    * [API](#api)
    * [Script](#script)
* [Test](#test)
    * [All](#all)
    * [Script Only](#script-only)



<!-- ABOUT THE PROJECT -->
## About The Project


### Built With

* Nodejs
* Fastify
* PostgreSQL
* Prisma
* Jest
* Docker


### Project Structure
```
├── prisma
│   │
│   ├── migrations              ( prisma migrations )
│   └── schema.prisma           ( prisma database schema )
│
├── src
│   │
│   ├── db-utils                ( database utilities like init, seed, ... )
│   │   ├── lib.js              ( core logic of the utilities )
│   │   ├── migrate.js      
│   │   ├── reset.js        
│   │   └── seed.js         
│   │
│   ├── plugins                 ( fastify plugins )
│   │   └── authPlugin.js       ( fastify plugin to get user id from headers)
│   │
│   ├── script                  ( the script logic and tests )       
│   │   ├── __tests__           ( the script tests)
│   │   │   ├── __snapshots__
│   │   │   └── script.test.js
│   │   ├── index.js            ( entry point of the script )
│   │   ├── input.txt           ( sample input for the script)
│   │   └── lib.js              ( core logic of the script )
│   │
│   ├── __tests__               ( api tests )
│   │   ├── __snapshots__
│   │   └── api.test.js
│   │
│   ├── config.js           
│   ├── index.js                ( entry point of the api )
│   └── lib.js                  ( core logic of the api )
│
├── Dockerfile
├── jest.config.js
├── package.json
├── package-lock.json
├── postman-collection.json
├── .gitlab-ci.yml              ( configuration for the ci/cd pipelien in gitlab )
├── .envrc                      ( script for direnv )
├── README.md
├── shell.nix                   ( nix-shell file )
└── tsconfig.json

```

### Process

The repo has three main brances `dev`, `stg`, `master` every push to any of these branchs triggers the ci to build and test the code then depoly to the right app on heroku based on the branch

- dev -> https://hussein-test-docker-dev.herokuapp.com
- stg -> https://hussein-test-docker-stg.herokuapp.com
- master -> https://hussein-test-docker.herokuapp.com

**use the above urls insted of localhost:3000 in postman collection if you don't want to run locally*

**also, you can choose in which enviroment you want to run the app from the run tab in vscode*

also an image is added to gitlab container registery for each build

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites

This is an example of how to list things you need to use the software and how to install them.
* nodejs (v13)
* npm
```sh
npm install npm@latest -g
```
* PostgreSQL

### Installation
 
1. Clone the repo
```sh
git clone https://gitlab.com/husseinraoouf/test.git
```
2. Install NPM packages
```sh
npm install
```
3. Install `@prisma/cli` globally
```sh
npm install --global @prisma/cli@2.0.0-alpha.1272
```
4. Intialize the database
```sh
npm run db:init
```
5. run ( you can run from vscode insted  )
```sh
npm run dev
```

<!-- USAGE EXAMPLES -->
## Usage

### API

Open The [Postman Collection](https://gitlab.com/husseinraoouf/test/-/blob/master/postman-collection.json)

### Script

The script is located at `src/script/index.js` you can run it from there directy or use it from npm scripts like this `npm run script -- <arguments>`

the script has two optiona arguments

```
-i          provide a path to input file (if not provided, the script takes the input from console until the word "end" is enterd )
            ex: npm run script -- -i ./src/script/input.txt

-o          provide a path to output file (if not provided, the script prints the output to the console )
            ex: npm run script -- -o ./src/script/output.txt
```

<!-- USAGE EXAMPLES -->
## Test

**Currently Test Coverage is 100% :tada:**

*You can add `-- --coverage` add the end of the follwong command if you want to generate test coverage report `ex: npm test -- --coverage`*

### All

run all tests
```
npm test
```

### Script Only

run script tests
```
npm run test:script
```
