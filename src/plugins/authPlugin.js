const fp = require('fastify-plugin');

module.exports = fp((fastify, opts, done) => 
{
    fastify.decorateRequest('user', {});

    fastify.addHook('preHandler', async (req, reply) => 
    {
        // This logic could easily be improved to use a better authraization schema like jwt ( Json Web Tokens )
        const userId = req.headers.authorization;
        if (userId) {
            req.user = {
                id: parseInt(userId)
            }
        }
        else {
            const err = new Error()
            err.statusCode = 401
            err.message = 'Unauthorized';
            throw err;
        }
    })

    done()
})