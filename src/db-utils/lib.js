
const util = require('util');
const exec = util.promisify(require('child_process').exec);

const seed = async (prisma) => {
    const users = [
        {
            id: 1,
            name: "Pet Owner",
            email: "user1@getnada.com"
        },
        {
            id: 2,
            name: "Bider 1",
            email: "user2@getnada.com"
        },
        {
            id: 3,
            name: "Bider 2",
            email: "user3@getnada.com"
        }
    ];

    for (const user of users) {
        await prisma.user.upsert({
            create: user,
            update: user,
            where: {
                id: user.id
            }
        })
    }


    const pets = [
        {
            id: 1,
            name: "Pet 1",
            user: {
                connect: {
                    id: 1
                }
            },
        },
    ];

    for (const pet of pets) {
        await prisma.pet.upsert({
            create: pet,
            update: pet,
            where: {
                id: pet.id
            }
        })
    }
}

const emptyDatabase = async (prisma) => {
    await prisma.bid.deleteMany();
    await prisma.pet.deleteMany();
    await prisma.user.deleteMany();

    // reset the autoincremented value of bid.id column to start with 1
    await prisma.raw(`ALTER SEQUENCE "Bid_id_seq" RESTART WITH 1;`)
}

const reset = async (prisma) => {
    await emptyDatabase(prisma)
    await seed(prisma)
}

const migrate = async (config) => {
    const { stdout, stderr } = await exec('npx prisma migrate up -c --experimental', {
        env: {
            ...config,
            ...process.env
        }
    });
    
    if (stderr) {
        console.error(stderr);
    }

    console.log(stdout);
}

const closeConnection = async (prisma) => {
    prisma.disconnect()
}

module.exports = {
    seed,
    emptyDatabase,
    reset,
    migrate,
    closeConnection
}
