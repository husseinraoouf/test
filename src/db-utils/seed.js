const { PrismaClient } = require('@prisma/client')
const { seed, closeConnection } = require('./lib')
const config = require('../config');

// temp workaround untill this issue is fixed: https://github.com/prisma/prisma/issues/2609 
process.env.DATABASE_URL = config.ActiveEnviroment.DATABASE_URL;
const prisma = new PrismaClient({
  datasources: {
      db: config.ActiveEnviroment.DATABASE_URL
  }
})

seed(prisma).catch(err => {
  throw err;
}).finally(() => {
  closeConnection(prisma);
});
