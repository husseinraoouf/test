const parseInput = (content) => {
    const newContent = content.trim();

    let lines = newContent.split('\n');

    // remove empty lines 
    lines = lines.map(line => line.trim()).filter(line => line);

    const numberOfItems = parseInt(lines[0].split(" ")[1])

    // remove first line because already read it
    lines = lines.slice(1);

    return {
        numberOfItems,
        content: lines.map(line => {
            const lineParts = line.split(/\s+(?=\d)/)
            return { user: lineParts[0], paid: parseInt(lineParts[1]) }
        }).sort((a, b) => (b.paid - a.paid) || a.user.localeCompare(b.user))
    };
}

const calculateOutput = (parsedContent) => {
    if (parsedContent.content.length == 0) {
        return {
            type: 'message',
            message: "No Winners"
        }
    }

    if (parsedContent.numberOfItems >= parsedContent.content.length) {
        return {
            type: 'data',
            data: parsedContent.content.slice(0, parsedContent.numberOfItems)
        }
    }


    const result = {
        type: 'data',
        data: []
    }

    const numberOfLoosers = parsedContent.content.length - parsedContent.numberOfItems;

    for (let i = 0; i < parsedContent.numberOfItems; i++) {
        result.data.push({
            user: parsedContent.content[i].user,
            paid: parsedContent.content[i+numberOfLoosers].paid
        })
    }

    for (let i = parsedContent.numberOfItems; i < parsedContent.content.length; i++) {
        result.data.push({
            user: parsedContent.content[i].user,
            paid: "Lost"
        })
    }

    return result;
}

const formatOutput = (outputData) => {
    if (outputData.type == 'message') {
        return outputData.message
    } else if (outputData.type == 'data') {
        return outputData.data.map(bider => `${bider.user}${" ".repeat(30-bider.user.length)}${bider.paid}`).join('\n')
    }
}

const processOneInput = (content) => {
    const parsedContent = parseInput(content);

    const outputData = calculateOutput(parsedContent);

    return formatOutput(outputData);
}

module.exports = {
    parseInput,
    calculateOutput,
    formatOutput,
    processOneInput
}
