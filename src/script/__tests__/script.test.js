const { processOneInput } = require('../lib')

test('script should work when number of item < biders', async () => {
    // Arrange
    const input = `
    Items 3
    John Doe        100
    John Smith      500
    Sara Conor      280
    Martin Fowler   320
    `

    // Act
    const result = processOneInput(input)

    // Assert
    expect(result).toMatchSnapshot();
});


test('script should work when number of item == biders', async () => {
    // Arrange
    const input = `
    Items 3
    John Doe        100
    Sara Conor      280
    Martin Fowler   320
    `

    // Act
    const result = processOneInput(input)

    // Assert
    expect(result).toMatchSnapshot();
});


test('script should work when number of item > biders', async () => {
    // Arrange
    const input = `
    Items 5
    John Doe        100
    John Smith      500
    Sara Conor      280
    Martin Fowler   320
    `

    // Act
    const result = processOneInput(input)

    // Assert
    expect(result).toMatchSnapshot();
});


test('script should work when no biders', async () => {
    // Arrange
    const input = `
    Items 3
    `

    // Act
    const result = processOneInput(input)

    // Assert
    expect(result).toMatchSnapshot();
});