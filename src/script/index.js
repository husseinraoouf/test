#!/usr/bin/env node

const fs = require('fs').promises;
const { argv } = require('yargs')
const readline = require('readline')

const { processOneInput } = require('./lib')

const start = async () => {
    let content = "";

    if (argv.i) {
        content = await fs.readFile(argv.i, 'utf-8');
    }
    else {
        const rl = readline.createInterface({
            input: process.stdin,
            output: process.stdout
        })
        for await (const line of rl) {
            if (line == "end") {
                break;
            }
    
            content += line;
            content += '\n';
        }
        rl.close();
        console.log('\n')
    }

    const outputFormated = processOneInput(content);

    if (argv.o) {
        await fs.writeFile(argv.o, outputFormated, { flag: 'w' })
    }
    else {
        console.log("*Output Formated*", '\n' + outputFormated, '\n');
    }

}

start()