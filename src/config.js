// Could be improved to get the configuration from and secrets manger like azure keyvault
const ENVIRONMENTS = {
    Local: {
        DATABASE_URL: "postgresql://postgres@localhost/dev-db",
    },
    Test: {
        DATABASE_URL: "postgresql://postgres@localhost/test",
    },
    Development: {
        DATABASE_URL: "postgres://aojgqztc:m-0YbscjskGmynnBKvsNEozSwuKQRtSi@kandula.db.elephantsql.com:5432/aojgqztc",
    },
    Staging: {
        DATABASE_URL: "postgres://zvdkiams:yXrPSS5ThPgP9uSreH0LYnP4gCdBxtcF@kandula.db.elephantsql.com:5432/zvdkiams",
    },
    Production: {
        DATABASE_URL: "postgres://nrgkirhg:s3lA7y3Wn_xv27P9MlnmGyimhHSrYy0S@kandula.db.elephantsql.com:5432/nrgkirhg",
    }
};

module.exports = {
    ActiveEnviroment: ENVIRONMENTS[process.env.ACTIVE_ENVIRONMENT || 'Local'],
    ...ENVIRONMENTS
}