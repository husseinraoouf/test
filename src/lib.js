const fastify = require('fastify')
const cors = require('fastify-cors')
const { NotFound, Unauthorized, Forbidden } = require('http-errors')

const authraizationPlugin = require('./plugins/authPlugin');

const createApp = (prisma, fastifyOption = {}) => {
    const app = fastify({
        logger: true,
        ...fastifyOption
    });

    app.register(cors)
    app.register(authraizationPlugin)

    app.post('/:petId/bid', async (req, res) => {

        const petId = parseInt(req.params.petId);

        const pet = await prisma.pet.findOne({
            where: { id: petId },
        })

        if (!pet) {
            throw new NotFound('Sorry, No pet with that Id')
        }

        if (pet.userId == req.user.id) {
            throw new Forbidden('You can\'t make a bid on your pet');
        }

        let bid = await prisma.bid.create({
            data: {
                user: {
                    connect: {
                        id: req.user.id
                    }
                },
                pet: {
                    connect: {
                        id: petId
                    }
                },
                amount: req.body.amount,
            },
            include: {
                user: true
            }
        })
        return res.code(201).send(bid)
    })

    app.get('/:petId/listBids', async (req, res) => {

        const petId = parseInt(req.params.petId);

        const pet = await prisma.pet.findOne({
            where: { id: petId },
            include: {
                bids: {
                    include: {
                        user: true
                    }
                }
            }
        })

        if (!pet || pet.userId != req.user.id) {
            throw new Unauthorized();
        }

        return res.send(pet.bids)
    })

    return app
}

module.exports = createApp
