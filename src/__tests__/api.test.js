const { PrismaClient } = require('@prisma/client')
const { migrate, reset, closeConnection } = require('../db-utils/lib');
const createApp = require('../lib');

const config = require('../config');

let prisma
let app

beforeAll(async () => {
    await migrate(config.Test);
    
    // temp workaround untill this issue is fixed: https://github.com/prisma/prisma/issues/2609 
    process.env.DATABASE_URL = config.Test.DATABASE_URL;
    prisma = new PrismaClient({
        datasources: {
            db: config.Test.DATABASE_URL
        }
    })
    
    await prisma.connect();

    app = createApp(prisma, { logger: false })
});

afterAll(async () => {
    await closeConnection(prisma);
    app.close()

    prisma = null
    app = null
});

beforeEach(async () => {
    await reset(prisma);
});

test('must provide id in authorization header', async () => {
    const response = await app.inject()
        .get('/1/listBids')

    expect(response.payload).toMatchSnapshot();
});

test('list bids should be intialy empty', async () => {
    const response = await app.inject()
        .get('/1/listBids')
        .headers({
            authorization: '1'
        })

    expect(response.payload).toMatchSnapshot();
});

test('only owner can list bids on a pet', async () => {
    const addResponse = await app.inject()
        .post('/1/bid')
        .headers({
            authorization: '2'
        })
        .body({
            amount: 300
        })

    const listReponse = await app.inject()
        .get('/1/listBids')
        .headers({
            authorization: '2'
        })

    expect(listReponse.payload).toMatchSnapshot();
});

test('add bid should work', async () => {
    const addResponse = await app.inject()
        .post('/1/bid')
        .headers({
            authorization: '2'
        })
        .body({
            amount: 300
        })

    const listReponse = await app.inject()
        .get('/1/listBids')
        .headers({
            authorization: '1'
        })

    expect(addResponse.payload).toMatchSnapshot();
    expect(listReponse.payload).toMatchSnapshot();
});


test('pet owner can\'t add bid', async () => {
    const response = await app.inject()
        .post('/1/bid')
        .headers({
            authorization: '1'
        })
        .body({
            amount: 300
        })

    expect(response.payload).toMatchSnapshot();
});


test('can\'t add bid on pet that doesn\'t exist', async () => {
    const response = await app.inject()
        .post('/20/bid')
        .headers({
            authorization: '2'
        })
        .body({
            amount: 300
        })

    expect(response.payload).toMatchSnapshot();
});

