const { PrismaClient } = require("@prisma/client")
const createApp = require('./lib');
const config = require('./config');


const start = async () => {
    // temp workaround untill this issue is fixed: https://github.com/prisma/prisma/issues/2609 
    process.env.DATABASE_URL = config.ActiveEnviroment.DATABASE_URL;
    const prisma = new PrismaClient({
        datasources: {
            db: config.ActiveEnviroment.DATABASE_URL
        }
    })

    const app = createApp(prisma);

    const port = process.env.PORT || 3000;
    app.listen(port, '0.0.0.0') // the '0.0.0.0' here to be able to listen on all hosts in docker image
        .then((address) => console.log(`server listening on ${address}`))

}

start()
