# Migration `20200527195316-add-bids-to-user-pets`

This migration has been generated at 5/27/2020, 7:53:16 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql

```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20200527194856-remove-autoincrement..20200527195316-add-bids-to-user-pets
--- datamodel.dml
+++ datamodel.dml
@@ -2,9 +2,9 @@
 // learn more about it in the docs: https://pris.ly/d/prisma-schema
 datasource sqlite {
   provider = "sqlite"
-  url = "***"
+  url      = "file:dev.db"
 }
 generator client {
   provider = "prisma-client-js"
@@ -14,15 +14,19 @@
   id      Int     @id
   email   String  @unique
   name    String
   pets    Pet[]
+
+  bids    Bid[]
 }
 model Pet {
   id     Int      @id
   name   String
   user   User     @relation(fields: [userId], references: [id])
   userId Int
+
+  bids    Bid[]
 }
 model Bid {
   id     Int      @default(autoincrement()) @id
```


