# Migration `20200527194856-remove-autoincrement`

This migration has been generated at 5/27/2020, 7:48:56 PM.
You can check out the [state of the schema](./schema.prisma) after the migration.

## Database Steps

```sql

```

## Changes

```diff
diff --git schema.prisma schema.prisma
migration 20200527165611-init..20200527194856-remove-autoincrement
--- datamodel.dml
+++ datamodel.dml
@@ -2,36 +2,36 @@
 // learn more about it in the docs: https://pris.ly/d/prisma-schema
 datasource sqlite {
   provider = "sqlite"
-  url = "***"
+  url      = "file:dev.db"
 }
 generator client {
   provider = "prisma-client-js"
 }
 model User {
-  id      Int      @default(autoincrement()) @id
-  email   String   @unique
+  id      Int     @id
+  email   String  @unique
   name    String
   pets    Pet[]
 }
 model Pet {
-  id     Int     @default(autoincrement()) @id
+  id     Int      @id
   name   String
-  user   User    @relation(fields: [userId], references: [id])
+  user   User     @relation(fields: [userId], references: [id])
   userId Int
 }
 model Bid {
-  id     Int     @default(autoincrement()) @id
+  id     Int      @default(autoincrement()) @id
-  pet    Pet    @relation(fields: [petId], references: [id])
+  pet    Pet      @relation(fields: [petId], references: [id])
   petId  Int
-  user   User    @relation(fields: [userId], references: [id])
+  user   User     @relation(fields: [userId], references: [id])
   userId Int
   amount Float
 }
```


